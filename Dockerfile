FROM openjdk:8-alpine

RUN apk update && \
  apk add bash && \
  rm -rf /var/cache/apk/*

WORKDIR /tmp

RUN wget https://github.com/rabbitmq/rabbitmq-perf-test/releases/download/v2.2.0/rabbitmq-perf-test-2.2.0-bin.tar.gz && \
  tar -zxvf rabbitmq-perf-test-2.2.0-bin.tar.gz

WORKDIR /tmp/rabbitmq-perf-test-2.2.0

ENTRYPOINT ["bin/runjava", "com.rabbitmq.perf.PerfTest"]
CMD ["--help"]
